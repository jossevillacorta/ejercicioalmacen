package clasesYobjetos;

public class Persona {
	public String nombre;
	public int edad;
	public String dni;
	public double peso;
	public double altura;
	public char sexo;
	
	
	public Persona() {
		
	}
	
	public Persona(String nombre,int edad, String dni, double peso, double altura, char sexo) {
		this.nombre=nombre;
		this.edad=edad;
		this.dni=dni;
		this.peso=peso;
		this.altura=altura;
		this.sexo=sexo;
	}

	public int calcularIMC() {
		double resultado = this.peso/(Math.pow(this.altura, 2));
		if (resultado < 20) {
			return -1;
		}else if (resultado < 25) {
			return 0;
		}else {
			return 1;
		}
	}

	public boolean esMayorDeEdad() {
		return this.edad>=18;
	}

	public String toString() {
		String sexo = "";
		if (this.sexo == 'f') {
			sexo = "Femenino";
		}else {
			sexo = "Masculino";
		}
		return this.nombre+" | "+this.edad+" años | DNI: "+this.dni+" | "+this.peso+" Kg | "+this.altura+" mts | Sexo: "+sexo+" |";
	}

	public String generarCuil () {
		int prefijo;
		if (this.sexo=='f') {
			prefijo = 27;
		}else {
			prefijo = 20;
		} 
		return ""+prefijo+"-"+this.dni+"-"+this.dni.charAt(this.dni.length()-1);
	}
}

package clasesYobjetos;

public class Viaje {
	private Fecha fecha;
	private String tipoTransporte;
	private String nombreTransporte;
	private double valorViaje;
	
	
	public Viaje (Fecha fecha, String tipoTransporte, String nombreTransporte, double valorViaje) {
	this.fecha=fecha;
	this.tipoTransporte=tipoTransporte;
	this.nombreTransporte=nombreTransporte;
	this.valorViaje=valorViaje;
	}
	
	public String toString() {
		return fecha+" | "+tipoTransporte+" | "+nombreTransporte+"| "+valorViaje;
	}

	}


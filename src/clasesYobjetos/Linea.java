package clasesYobjetos;

import java.awt.Point;

public class Linea {
	Point puntoA = new Point();
	Point puntoB = new Point();
	
	public Linea () {
		 this.puntoA.x=0;
		 this.puntoA.y=0;
		 this.puntoB.x=0;
		 this.puntoB.y=0;
		 
	}
	
	public Linea (Point punto1, Point punto2) {
		this.puntoA=punto1;
		this.puntoB=punto2;
		
	}
	
	public void mueveDerecha (double n) {
		if (n>0) {
			this.puntoA.x+=n;
			this.puntoB.x+=n;
		}
	}

	public void mueveIzquierda (double n) {
		if (n>0) {
			this.puntoA.x-=n;
			this.puntoB.x-=n;
		}
	}

	public void mueveArriba (double n) {
		if (n>0) {
			this.puntoA.y-=n;
			this.puntoB.y-=n;
			
		}
	}

	public void mueveAbajo (double n) {
		if (n>0) {
			this.puntoA.y+=n;
			this.puntoB.y+=n;
		}
	}

	public static void mostrasPuntos (Point puntoA, Point puntoB) {
		System.out.println("[("+puntoA.x+","+puntoA.y+");("+puntoB.x+","+puntoB.y+")]");
	}

}
	

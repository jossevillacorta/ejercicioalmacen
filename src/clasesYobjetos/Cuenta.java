package clasesYobjetos;

public class Cuenta {
	private String titular;
	private double saldo;
	
	public Cuenta (String titular, double saldo) {
		this.titular = titular;
		this.saldo = saldo;
	}
	
	public Cuenta (String titular) {
		this.titular = titular;
		this.saldo = 0;
	}
	
	public void ingresar(double cantidad) {
		if (cantidad>0) {
			this.saldo+=cantidad; 
		}
	}

	public void retirar(double cantidad) {
		if (this.saldo-cantidad>0) {
			this.saldo-=cantidad;
		}
	}

	public void mostrarSaldo () {
		System.out.println("Su saldo es de: $"+this.saldo);
	}
}

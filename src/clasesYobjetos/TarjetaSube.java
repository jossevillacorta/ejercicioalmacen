package clasesYobjetos;

public class TarjetaSube {
	private int numId;
	private double saldo;
	private Viaje[] ultimosViajes;
	private int cantViajes;
	
	public TarjetaSube (int numId) {
		this.numId=numId;
		this.saldo=100;
		this.ultimosViajes = new Viaje [50];
		this.cantViajes = 0;
	}
	
	
	public  String toString() {
		return "Tarjeta num: "+this.numId+" Saldo: $"+this.saldo;
	}


	public int getNumId() {
		return numId;
	}


	public double mostrarSaldo() {
		if (this.saldo<0) {
			return 0;
		}else {
		return this.saldo;
		}
	}

	public void actualizarSaldo (double monto) {
		if (this.saldo-monto>-50) {
			this.saldo-=monto;
			System.out.println("Abonado con éxito, gracias por viajar");
		}else {
			System.out.println("Saldo insuficiente");
		}

		
	}

	public void mostrarViajes() {
		for (int i=0; i<this.cantViajes; i++) {
			int cant=i+1;
			System.out.println("["+cant+"] "+this.ultimosViajes[i]);
		}
	}

	public void actualizarRegistro(Viaje viaje) {
		
		if (this.cantViajes==50) {
			for (int i=1; i<this.ultimosViajes.length;i++) {
				this.ultimosViajes[i-1]=this.ultimosViajes[i];
			}
			this.ultimosViajes[49]=viaje;
			
		}else {
			this.ultimosViajes[this.cantViajes]=viaje;
			this.cantViajes++;
		}
		
		
	}

	public void cargarSube(double monto) {
		this.saldo+=monto;
	}
}



package clasesYobjetos;

public class Producto {
	private String tipo;
	private int numId;
	private double litros;
	private double precio;
	private String marca;
	private double cantAzucar;

	public Producto (String tipo, int numId, double litros, double precio, String marca) {
		this.tipo = tipo;
		this.numId = numId;
		this.litros = litros;
		this.precio = precio;
		this.marca = marca;
		this.cantAzucar = 0;
		if (!this.esValida()) {
			throw new RuntimeException ("Reviar tipo de bebida y cantidad de azucar");
		}
	}
	public Producto(String tipo, int numId, double litros, double precio, String marca, double cantAzucar) {
		this.tipo = tipo;
		this.numId = numId;
		this.litros = litros;
		this.precio = precio;
		this.marca = marca;
		this.cantAzucar = cantAzucar;
		if (!this.esValida()) {
			throw new RuntimeException ("Reviar tipo de bebida y cantidad de azucar");
		}
	}
	public boolean esValida() {
		return ((this.tipo.equals("agua mineral") && this.cantAzucar==0) || (this.tipo.equals("bebida azucarada") && this.cantAzucar>0));
	}
	
	public String toString() {
		if (this.cantAzucar>0) {
			return this.tipo+" | ID: "+this.numId+" | "+this.litros+" lts | $"+this.precio+" | "+this.marca+" | "+this.cantAzucar+" grs";
		} else {
			return this.tipo+" | ID: "+this.numId+" | "+this.litros+" lts | $"+this.precio+" | "+this.marca;
		}
		
	}
	public double getLitros() {
		return litros;
	}
	public void setLitros(double litros) {
		this.litros = litros;
	}
	public String getTipo() {
		return tipo;
	}
	public int getNumId() {
		return numId;
	}
	public double getPrecio() {
		return precio;
	}
	public String getMarca() {
		return marca;
	}
	public double getCantAzucar() {
		return cantAzucar;
	}
}
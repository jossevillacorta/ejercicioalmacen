package clasesYobjetos;

public class Fecha {
    //Variables de instacia.
	int dia;
	int mes;
	int anio;
	
	//Constructores
	public Fecha () {
		this.dia=1;
		this.mes=1;
		this.anio=0;
	}
	
	public Fecha(int i, int j, int k) {
		this.dia=i;
		this.mes=j;
		this.anio=k;
	}

	
//  Devuelve la cantidad de dias del mes dado en el año dado.
	public static int diasDelMes(int mes, int anio) {
		if (mes==1 || mes==3 || mes==5 || mes==7 || mes==8 || mes==10 || mes==12) {
			return 31;
		} else if (mes==4 || mes==6 || mes==9 || mes==11) {
			return 30;
		} else {
			return (esBisiesto(anio)) ? 29 : 28;
		}
	}
	
	
//  Devuelve verdadero si la fecha es válida y falso en caso contrario.
	public boolean esValida() {
		if (this.dia<=0 || this.dia>diasDelMes(this.mes,this.anio)) {
			return false;
		}else if (this.mes<=0 || this.mes>12) {
			return false;
		}else if (this.anio<0) {
			return false;
		}
		return true;
		
	}	

	
//  Avanza un día de la fecha.
	public void avanzarDia() {
		if (this.dia<diasDelMes(this.mes,this.anio)) {
			this.dia++;
		}
		else if (this.dia==diasDelMes(this.mes,this.anio) && this.mes<12) {
			this.dia=1;
			this.mes++;
		}else {
			this.dia=1;
			this.mes=1;
			this.anio++;
		}		
	}
	
//	Retrocede un día de la fecha del parametro implicito.
	public void retrocederDia() {
		if (this.dia>1) {
			this.dia--;
		}
		else if (this.dia==1 && this.mes>1) {
			this.dia=diasDelMes(this.mes, this.anio);
			this.mes--;
		}else {
			this.dia=diasDelMes(this.mes, this.anio);
			this.mes=12;
			this.anio--;
		}		
	}

//  Devuelve verdadero si la fecha es anterior a la fecha recibida como parámetro.
	public boolean antesQue(Fecha otra) {
		if (this.anio<otra.anio) {
			return true;
		}else if (this.anio==otra.anio && this.mes<otra.mes) {
			return true;
		}else if (this.mes==otra.mes && this.dia<otra.dia) {
			return true;
		}return false;
	}
	
    
//  Devuelve el número de dia en el año que representa la fecha (debe ser un número ente 1 y 366).
	public int diaDelAnio() {
		int cant = 0;
		for (int i=1; i<this.mes;i++) {
			cant += diasDelMes(i,this.anio);
		}
		return cant+this.dia;
	}
	
//  Cuenta la cantidad de días de diferencia entre la fecha implícita y la fecha recibida como parámetro
	public int diasDeDiferenciaCon(Fecha otra) {
		int cont = 0;
		if (this.antesQue(otra)) {
			while (this.equals(otra)==false) {
				this.avanzarDia();
				cont++;
			}
		}
		else {
			while (this.equals(otra)==false) {
			this.retrocederDia();
			cont++;
			}
		}
		return cont;
	}

	
		private static boolean esBisiesto(int a) {
			if (a % 4 == 0 && a % 100 != 0) {
				return true;
			}
			return false;
	
	}

		public void imprimirFecha () {
			System.out.println(this.dia +"/"+this.mes+"/"+this.anio);
		}

		public boolean equals(Fecha otra) {
			return this.dia==otra.dia && this.mes==otra.mes && this.anio==otra.anio;
		}
		
		public String toString() {
			return this.dia+"/"+this.mes+"/"+this.anio;
		}
}
package clasesYobjetos;

public class Almacen {
	private Producto[][] estanterias;
	private int fila;
	private int columna;

	public Almacen(int fila, int columna) {
		this.fila = fila;
		this.columna = columna;
		this.estanterias = new Producto[fila][columna];
	}

	public Almacen() {
		this.fila = 3;
		this.columna = 3;
		this.estanterias = new Producto[fila][columna];
	}

	public void mostrarAlmacen() {
		for (int i = 0; i < fila; i++) {
			for (int j = 0; j < columna; j++) {
				System.out.println(estanterias[i][j]);
			}

		}
	}

	public void agregarProducto(Producto producto) {
		for (int i = 0; i < this.fila; i++) {
			for (int j = 0; j < this.columna; j++) {
				if (this.estanterias[i][j] != null && this.estanterias[i][j].getNumId() == producto.getNumId()) {
					System.out.println("El articulo ya se encuentra en estanteria");
					return;
				}
			}
		}
		for (int k = 0; k < this.fila; k++) {
			for (int l = 0; l < this.columna; l++) {
				if (this.estanterias[k][l] == null) {
					this.estanterias[k][l] = producto;
					return;
				}
			}
		}
	}

	public void quitarProducto (int num) {
		for (int i=0; i<this.fila; i++) {
			for (int j=0; j<this.columna; j++) {
				if (this.estanterias[i][j] != null & this.estanterias[i][j].getNumId()==num) {
					this.estanterias[i][j] = null;
					return;
				}
			}
		}
	}
	
	public double precioTotal() {
		double costo = 0;
		for (int i = 0; i < this.fila; i++) {
			for (int j = 0; j < this.columna; j++) {
				costo += this.estanterias[i][j].getPrecio();
			}
		}
		return costo;
	}

	public double precioTotalMarca(String marca) {
		double costo = 0;
		for (int i = 0; i < this.fila; i++) {
			for (int j = 0; j < this.columna; j++) {
				if (this.estanterias[i][j].getMarca().equals(marca)) {
					costo += this.estanterias[i][j].getPrecio();
				}
			}
		}
		return costo;
	}

	public double precioEstanteria(int n) {
		double costo = 0;
		for (int i = 0; i < this.fila; i++) {
			costo += this.estanterias[i][n].getPrecio();
		}
		return costo;
	}
}